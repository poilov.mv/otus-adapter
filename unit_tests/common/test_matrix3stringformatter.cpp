#include "gtest/gtest.h"

#include "Matrix3StringFormatter.h"

#include <fstream>
#include <sstream>


TEST(TestMatrix3StringFormatter, can_format_matrix_to_oneline_string) {
    std::vector<double> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto m = Matrix3::fromVector(v);

    auto result = Matrix3StringFormatter::toString(m);

    ASSERT_EQ("1 2 3 4 5 6 7 8 9", result);
}

TEST(TestMatrix3StringFormatter, can_format_matrix_to_oneline_string_with_decimal_point) {
    std::vector<double> v{ 1, 2.1, 3, 4.6, 5, 6.78, 7, 8, 9 };
    auto m = Matrix3::fromVector(v);

    auto result = Matrix3StringFormatter::toString(m);

    ASSERT_EQ("1 2.1 3 4.6 5 6.78 7 8 9", result);
}

TEST(TestMatrix3StringFormatter, can_parse_string_to_matrix) {
    std::vector<double> v{ 1, 2.1, 3, 4.6, 5, 6.78, 7, 8, 9 };
    Matrix3 m;
    
    auto result = Matrix3StringFormatter::fromString("1 2.1 3 4.6 5 6.78 7 8 9", &m);

    ASSERT_TRUE(result);
    ASSERT_EQ(m.toVector(), v);
}

TEST(TestMatrix3StringFormatter, cant_parse_wrong_string_to_matrix) {
    Matrix3 m;

    auto result = Matrix3StringFormatter::fromString("1 2.1 3 4.6 5 6.78 7 8", &m);

    ASSERT_FALSE(result);
}

TEST(TestMatrix3StringFormatter, cant_parse_realy_wrong_string_to_matrix) {
    Matrix3 m;
    auto result = Matrix3StringFormatter::fromString("text", &m);

    ASSERT_FALSE(result);
}

TEST(TestMatrix3StringFormatter, can_format_two_matricies_to_string) {
    std::vector<double> v1{ 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    auto m1 = Matrix3::fromVector(v1);
    std::vector<double> v2{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto m2 = Matrix3::fromVector(v2);

    auto result = Matrix3StringFormatter::toString(m1, m2);

    ASSERT_EQ("9 8 7 6 5 4 3 2 1\n1 2 3 4 5 6 7 8 9", result);
}

TEST(TestMatrix3StringFormatter, can_parse_string_to_two_matricies) {
    std::vector<double> v1{ 1, 2.1, 3, 4.6, 5, 6.78, 7, 8, 9 };
    Matrix3 m1;
    std::vector<double> v2{ 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    Matrix3 m2;

    auto result = Matrix3StringFormatter::fromString("1 2.1 3 4.6 5 6.78 7 8 9\n9 8 7 6 5 4 3 2 1", &m1, &m2);

    ASSERT_TRUE(result);
    ASSERT_EQ(m1.toVector(), v1);
    ASSERT_EQ(m2.toVector(), v2);
}

TEST(TestMatrix3StringFormatter, cant_parse_wrong_string_to_two_matricies) {
    Matrix3 m1;
    Matrix3 m2;

    auto result = Matrix3StringFormatter::fromString("1 2.1 3 4.6 5 6.78 7 8\n9 8", &m1, &m2);

    ASSERT_FALSE(result);
}

TEST(TestMatrix3StringFormatter, cant_parse_empty_string_to_two_matricies) {
    Matrix3 m1;
    Matrix3 m2;

    auto result = Matrix3StringFormatter::fromString("", &m1, &m2);

    ASSERT_FALSE(result);
}
