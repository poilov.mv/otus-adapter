#include "gtest/gtest.h"

#include "main_app.h"
#include "TextFile.h"
#include "Matrix3StringFormatter.h"


class TestMain : public ::testing::Test {
protected:
    TextFile targetFile;
    TextFile runableFile;

    TestMain()
        : targetFile("target.txt")
        , runableFile("some.cmd") {
    }

    void TearDown() override {
        targetFile.remove();
        runableFile.remove();
    }

    int run_main(
        const std::string &operation = std::string(),
        const std::string &sourceFilePath = std::string()
        ) {
        std::string pr("program");
        std::string op(operation);
        std::string sp(sourceFilePath);

        int count = 1
            + (!op.empty() ? 1 : 0)
            + (!sp.empty() ? 1 : 0);

        char *argv[] = { &pr.at(0), 
            op.empty() ? NULL : &op.at(0),
            sp.empty() ? NULL : &sp.at(0) };
        return main_app(count, argv);
    }
};


TEST_F(TestMain, can_generate_matrices_to_file) {
    ASSERT_EQ(0, run_main(targetFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    Matrix3 matrix1, matrix2;
    ASSERT_TRUE(Matrix3StringFormatter::fromString(resultContent, &matrix1, &matrix2));
}

TEST_F(TestMain, show_synopsis_and_returns_error_without_args) {
    ASSERT_EQ(1, run_main());
}

TEST_F(TestMain, can_use_runable_for_transform) {
#ifdef linux
    runableFile.setContent("echo very good> \"$1\"");
#else
    runableFile.setContent("echo very good> \"%1\"");
#endif
    runableFile.write();

    ASSERT_EQ(0, run_main(targetFile.path(), runableFile.path()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "very good\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestMain, returns_error_with_wrong_target_file) {
    auto targetPath = "wrong_file_name_?*/.txt";
    ASSERT_EQ(2, run_main(targetPath));
}
