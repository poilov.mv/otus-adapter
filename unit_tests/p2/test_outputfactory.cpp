#include "gtest/gtest.h"

#include "OutputFactory.h"
#include "FileTargetOutput.h"
#include "RunableTargetOutput.h"


TEST(TestOutputFactory, can_create_file_output) {
    auto program = std::string("p2");
    auto filepath = std::string("filepath");
    char *argv[] = { &program.at(0), &filepath.at(0) };
    ArgumentParser parser(2, argv);

    auto output = createOutputTarget(parser);

    auto rightType = dynamic_cast<FileTargetOutput*>(output.get());
    ASSERT_TRUE(rightType);
}

TEST(TestOutputFactory, can_create_runable_output) {
    auto program = std::string("p2");
    auto filepath = std::string("filepath");
    auto runable = std::string("runable");
    char *argv[] = { &program.at(0), &filepath.at(0), &runable.at(0) };
    ArgumentParser parser(3, argv);

    auto output = createOutputTarget(parser);

    auto rightType = dynamic_cast<RunableTargetOutput*>(output.get());
    ASSERT_TRUE(rightType);
}
