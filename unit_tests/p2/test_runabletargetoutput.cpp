#include "gtest/gtest.h"

#include "RunableTargetOutput.h"
#include "TextFile.h"


class TestRunableTargetOutput : public ::testing::Test {
protected:
    TextFile targetFile;
    TextFile runableFile;

    TestRunableTargetOutput()
        : targetFile("target.txt")
        , runableFile("runable.cmd") {}

    void TearDown() {
        targetFile.remove();
        runableFile.remove();
    }
};


TEST_F(TestRunableTargetOutput, sum_matrix_from_one_file_to_another) {
#ifdef linux
    runableFile.setContent("echo good> \"$1\"");
#else
    runableFile.setContent("echo good> \"%1\"");
#endif
    runableFile.write();
    RunableTargetOutput output(runableFile.path());
    auto matrix = Matrix3::fromVector({ 1, 2, 4, 8, 10, 12, 16, 18, 20 });

    output.write(matrix, matrix, targetFile.path());

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "good\n";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestRunableTargetOutput, raise_error_if_can_not_save) {
    RunableTargetOutput output(runableFile.path());
    auto matrix = Matrix3::fromVector({ 1, 2, 4, 8, 10, 12, 16, 18, 20 });

    ASSERT_THROW({
        output.write(matrix, matrix, "wrong_file/?*.txt");
        }, TargetOutputExeption);
}

TEST_F(TestRunableTargetOutput, raise_error_if_runable_not_found) {
    RunableTargetOutput output(runableFile.path());
    auto matrix = Matrix3::fromVector({ 1, 2, 4, 8, 10, 12, 16, 18, 20 });

    ASSERT_THROW({
        output.write(matrix, matrix, targetFile.path());
        }, TargetOutputExeption);
}

TEST_F(TestRunableTargetOutput, raise_error_if_runable_returns_nonzero) {
    runableFile.setContent("exit 2");
    runableFile.write();
    RunableTargetOutput output(runableFile.path());
    auto matrix = Matrix3::fromVector({ 1, 2, 4, 8, 10, 12, 16, 18, 20 });

    ASSERT_THROW({
        output.write(matrix, matrix, targetFile.path());
        }, TargetOutputExeption);
}
