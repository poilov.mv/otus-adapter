#include "gtest/gtest.h"

#include "FileTargetOutput.h"
#include "TextFile.h"


class TestFileTargetOutput : public ::testing::Test {
protected:
    TextFile targetFile;

    TestFileTargetOutput()
        : targetFile("target.txt") {}

    void TearDown() {
        targetFile.remove();
    }
};


TEST_F(TestFileTargetOutput, sum_matrix_from_one_file_to_another) {
    FileTargetOutput output;
    auto matrix1 = Matrix3::fromVector({ 1, 2, 4, 8, 10, 12, 16, 18, 20 });
    auto matrix2 = Matrix3::fromVector({ 1, 2, 3, 4, 5, 6, 7, 8, 9 });

    output.write(matrix1, matrix2, targetFile.path());

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "1 2 4 8 10 12 16 18 20\n1 2 3 4 5 6 7 8 9";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestFileTargetOutput, raise_error_if_can_not_save) {
    FileTargetOutput output;
    auto matrix = Matrix3::fromVector({ 1, 2, 4, 8, 10, 12, 16, 18, 20 });

    ASSERT_THROW({
        output.write(matrix, matrix, "wrong_file/?*.txt");
        }, TargetOutputExeption);
}
