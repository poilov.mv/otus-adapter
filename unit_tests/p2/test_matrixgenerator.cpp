#include "gtest/gtest.h"

#include "MatrixGenerator.h"
#include <algorithm>


TEST(TestMatrixGenerator, test_can_generate_matrix3) {
    auto matrix = generateMatrix3();
    auto v = matrix.toVector();

    auto result = std::any_of(v.begin(), v.end(), [] (double item) {
        return item != 0;
    });

    ASSERT_TRUE(result);
}

TEST(TestMatrixGenerator, test_two_generated_matrices_are_different) {
    auto matrix1 = generateMatrix3();
    auto matrix2 = generateMatrix3();

    ASSERT_NE(matrix1.toVector(), matrix2.toVector());
}
