#include "gtest/gtest.h"

#include "ArgumentParser.h"


class TestArgumentParser : public ::testing::Test {
protected:
    ArgumentParser createParser(
        const std::string &arg1 = std::string(),
        const std::string &arg2 = std::string()
    ) {
        std::string p("program");
        std::string a1(arg1);
        std::string a2(arg2);

        int count = 1
            + (!a1.empty() ? 1 : 0)
            + (!a2.empty() ? 1 : 0);

        char *argv[] = { &p.at(0),
            a1.empty() ? NULL : &a1.at(0),
            a2.empty() ? NULL : &a2.at(0) };

        return ArgumentParser(count, argv);
    }
};

TEST_F(TestArgumentParser, can_parse_one_args) {
    auto parser = createParser("filepath");

    ASSERT_TRUE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, can_parse_two_args) {
    auto parser = createParser("filepath", "exepath");

    ASSERT_TRUE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, cant_parse_without_args) {
    auto parser = createParser();

    ASSERT_FALSE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, can_get_filepath) {
    auto parser = createParser("filepath");

    ASSERT_EQ(std::string("filepath"), parser.targetFilePath());
}

TEST_F(TestArgumentParser, can_get_runable) {
    auto parser = createParser("filepath", "exepath");

    ASSERT_TRUE(parser.hasRunable());
    ASSERT_EQ(std::string("exepath"), parser.runablePath());
}

TEST_F(TestArgumentParser, cant_get_runable) {
    auto parser = createParser("filepath");

    ASSERT_FALSE(parser.hasRunable());
}

TEST_F(TestArgumentParser, can_get_synopsis) {
    auto parser = createParser("path", "path");

    auto synopsis = parser.synopsis();

    ASSERT_FALSE(synopsis.empty());
}
