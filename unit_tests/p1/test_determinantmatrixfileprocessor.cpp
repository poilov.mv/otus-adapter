#include "gtest/gtest.h"

#include "DeterminantMatrixFileProcessor.h"
#include "TextFile.h"


class TestDeterminantMatrixFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;

    TestDeterminantMatrixFileProcessor()
        : sourceFile("source.txt")
        , targetFile("target.txt") {}

    void TearDown() {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestDeterminantMatrixFileProcessor, determinant_matrix_from_one_file_to_another) {
    DeterminantMatrixFileProcessor operation;
    sourceFile.setContent("1 2 3 4 5 6 7 8 9");
    sourceFile.write();

    operation.apply(sourceFile.path(), targetFile.path());

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "0";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestDeterminantMatrixFileProcessor, raise_error_if_error_in_source_file_format) {
    DeterminantMatrixFileProcessor operation;
    sourceFile.setContent("1 2 3 4 5");
    sourceFile.write();

    ASSERT_THROW({
        operation.apply(sourceFile.path(), targetFile.path());
        }, MatrixFileProcessorError);
}
