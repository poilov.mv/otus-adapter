#include "gtest/gtest.h"

#include "TransposeMatrixFileProcessor.h"
#include "TextFile.h"


class TestTransposeMatrixFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;

    TestTransposeMatrixFileProcessor()
        : sourceFile("source.txt")
        , targetFile("target.txt") {}

    void TearDown() {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestTransposeMatrixFileProcessor, transpose_matrix_from_one_file_to_another) {
    TransposeMatrixFileProcessor operation;
    sourceFile.setContent("1 2 3 4 5 6 7 8 9");
    sourceFile.write();

    operation.apply(sourceFile.path(), targetFile.path());

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "1 4 7 2 5 8 3 6 9";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestTransposeMatrixFileProcessor, raise_error_if_error_in_source_file_format) {
    TransposeMatrixFileProcessor operation;
    sourceFile.setContent("1 2 3 4 5");
    sourceFile.write();

    ASSERT_THROW({
        operation.apply(sourceFile.path(), targetFile.path());
        }, MatrixFileProcessorError);
}
