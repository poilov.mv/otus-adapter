#include "gtest/gtest.h"

#include "MatrixOperations.h"

#include "TransposeMatrixFileProcessor.h"
#include "DeterminantMatrixFileProcessor.h"
#include "SumMatrixFileProcessor.h"

#include "TransposeMatrixFileProcessorFactory.h"
#include "DeterminantMatrixFileProcessorFactory.h"
#include "SumMatrixFileProcessorFactory.h"

#include <algorithm>


TEST(TestMatrixOperations, operations_contains_transpose_fabric) {
    TransposeMatrixFileProcessorFactory fabric;
    MatrixOperations operations;
    auto ops = operations.operations();

    auto exists = std::find(ops.begin(), ops.end(), fabric.operation()) != ops.end();

    ASSERT_TRUE(exists);
}

TEST(TestMatrixOperations, operations_contains_determinant_fabric) {
    DeterminantMatrixFileProcessorFactory fabric;
    MatrixOperations operations;
    auto ops = operations.operations();

    auto exists = std::find(ops.begin(), ops.end(), fabric.operation()) != ops.end();

    ASSERT_TRUE(exists);
}

TEST(TestMatrixOperations, operations_contains_sum_fabric) {
    SumMatrixFileProcessorFactory fabric;
    MatrixOperations operations;
    auto ops = operations.operations();

    auto exists = std::find(ops.begin(), ops.end(), fabric.operation()) != ops.end();

    ASSERT_TRUE(exists);
}

TEST(TestMatrixOperations, can_create_transpose_operation) {
    TransposeMatrixFileProcessorFactory fabric;
    MatrixOperations operations;
    auto operation = operations.create(fabric.operation());

    auto rightType = dynamic_cast<TransposeMatrixFileProcessor*>(operation.get());
    ASSERT_TRUE(rightType);
}

TEST(TestMatrixOperations, can_create_determinant_operation) {
    DeterminantMatrixFileProcessorFactory fabric;
    MatrixOperations operations;
    auto operation = operations.create(fabric.operation());

    auto rightType = dynamic_cast<DeterminantMatrixFileProcessor*>(operation.get());
    ASSERT_TRUE(rightType);
}

TEST(TestMatrixOperations, can_create_sum_operation) {
    SumMatrixFileProcessorFactory fabric;
    MatrixOperations operations;
    auto operation = operations.create(fabric.operation());

    auto rightType = dynamic_cast<SumMatrixFileProcessor*>(operation.get());
    ASSERT_TRUE(rightType);
}

TEST(TestMatrixOperations, wrong_type_throws_exception) {
    MatrixOperations operations;
    
    ASSERT_THROW({
        auto operation = operations.create("something wrong"); }, 
        MatrixFileProcessorError);
}
