#include "gtest/gtest.h"

#include "SumMatrixFileProcessor.h"
#include "TextFile.h"


class TestSumMatrixFileProcessor : public ::testing::Test {
protected:
    TextFile sourceFile;
    TextFile targetFile;

    TestSumMatrixFileProcessor()
        : sourceFile("source.txt")
        , targetFile("target.txt") {}

    void TearDown() {
        sourceFile.remove();
        targetFile.remove();
    }
};


TEST_F(TestSumMatrixFileProcessor, sum_matrix_from_one_file_to_another) {
    SumMatrixFileProcessor operation;
    sourceFile.setContent("1 2 3 4 5 6 7 8 9\n1 2 3 4 5 6 7 8 9");
    sourceFile.write();

    operation.apply(sourceFile.path(), targetFile.path());

    targetFile.read();
    auto resultContent = targetFile.getContent();
    auto expectedContent = "2 4 6 8 10 12 14 16 18";
    ASSERT_EQ(expectedContent, resultContent);
}

TEST_F(TestSumMatrixFileProcessor, raise_error_if_error_in_source_file_format) {
    SumMatrixFileProcessor operation;
    sourceFile.setContent("1 2 3 4 5");
    sourceFile.write();

    ASSERT_THROW({
        operation.apply(sourceFile.path(), targetFile.path());
        }, MatrixFileProcessorError);
}
