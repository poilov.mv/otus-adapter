[![pipeline status](https://gitlab.com/poilov.mv/otus-adapter/badges/master/pipeline.svg)](https://gitlab.com/poilov.mv/otus-adapter/-/commits/master)
[![coverage report](https://gitlab.com/poilov.mv/otus-adapter/badges/master/coverage.svg)](https://gitlab.com/poilov.mv/otus-adapter/-/commits/master)

# Шаблоны проектирования

## Домашнее задание по теме **Адаптер и мост**
Адаптер для работы двух независимых программ. Описание применения шаблона в проекте
### Цель:
Написать простую консольную программу П1, с интерфейсом вызова И1, которая читает данные о двух матрицах А и В из файла F0, складывает матрицы и сохраняет результат А+В в другой файл F1.
Написать вторую консольную программу П2, которая может генерить данные матриц А и В и писать их в файл с именем F2.
Чтобы она могла их просуммировать, следует сделать адаптер для программы П1, который позволит программе П2 вызвать П1.
1. Написать программу П1
2. Написать программу П2, включив туда адаптер вызова и использования программы П1
3. Написать автотест для проверки функционирования
4. Если потребуется использовать адаптер в проектной работе, предоставить описание в текстовом файле в GitHub репозитории где конкретно и в какой роли используется этот шаблон.
5. нарисовать диаграмму классов.

## Пояснение к реализации
* В качестве программы П1 выбрана программа из ДЗ по шаблонному методу: программа выполняющая одну из трех операции над матрицами
* В программе П2 выбран самый простой интерфейс И1: на вход программы передается один аргумент - путь к файлу, над которым необходимо выполнить операцию
* Интерфейс И1, который реализован в адаптере программе П2, не совпадает с интерфейсом программы П1. Для устранения этой проблемы реализован адаптер через командный файл оболочки.
* Командный файл оболочки реализует интерфейс И1 и на вход принимает один аргумент из адаптера программы П2. Внутри себя он вызывает программу П1 уже с тремя необходимыми ей параметрами.
* В итоге получился двойной адаптер
* Данный подход продемонстрирован в функциональном тесте в файле: **func_tests\test_home_work.cpp**

## Диаграмма классов
### Программа 2
#### Наследование
```mermaid
classDiagram
    class ITargetOutput {
      write(matrix1, matrix2, filePath)
    }
    class FileTargetOutput {
      write(matrix1, matrix2, filePath)
    }
    class RunableTargetOutput {
      write(matrix1, matrix2, filePath)
    }

    FileTargetOutput --|> ITargetOutput
    RunableTargetOutput --|> ITargetOutput
    RunableTargetOutput *.. FileTargetOutput : использует
```
#### Вспомогательные классы
```mermaid
classDiagram
  class ArgumentParser {
    list<string> args
    +isArgsValid() bool
    +operation() string
    +sourceFilePath() string
    +targetFilePath() string
    +synopsis() string
  }
```

### Программа 1
#### Наследование
```mermaid
classDiagram
    class TransposeMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class SumMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class DeterminantMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class IMatrixFileProcessorFactory {
       +create() AbstractMatrixFileProcessor
       +operation() string
     }

    TransposeMatrixFileProcessorFactory --|> IMatrixFileProcessorFactory
    SumMatrixFileProcessorFactory --|> IMatrixFileProcessorFactory
    DeterminantMatrixFileProcessorFactory --|> IMatrixFileProcessorFactory

    AbstractMatrixFileProcessor <|-- TransposeMatrixFileProcessor
    AbstractMatrixFileProcessor <|-- SumMatrixFileProcessor
    AbstractMatrixFileProcessor <|-- DeterminantMatrixFileProcessor

    IMatrixFileProcessorFactory ..> AbstractMatrixFileProcessor : использует

    class AbstractMatrixFileProcessor {
      +apply(sourceFilePath, targetFilePath)
      -readFile(sourceFilePath) : string
      -writeFile(content, targetFilePath)
      #applyOp(content) : string
    }
    class TransposeMatrixFileProcessor {
      #applyOp(content) : string
    }
    class SumMatrixFileProcessor {
      #applyOp(content) : string
    }
    class DeterminantMatrixFileProcessor {
      #applyOp(content) : string
    }
```
#### Создание
```mermaid
classDiagram
    class TransposeMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class SumMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }
    class DeterminantMatrixFileProcessorFactory {
      +create() AbstractMatrixFileProcessor
      +operation() string
    }

    TransposeMatrixFileProcessorFactory --> TransposeMatrixFileProcessor : создает
    SumMatrixFileProcessorFactory --> SumMatrixFileProcessor : создает
    DeterminantMatrixFileProcessorFactory --> DeterminantMatrixFileProcessor : создает

    class TransposeMatrixFileProcessor {
      #applyOp(content) : string
    }
    class SumMatrixFileProcessor {
      #applyOp(content) : string
    }
    class DeterminantMatrixFileProcessor {
      #applyOp(content) : string
    }
```
#### Вспомогательные классы
```mermaid
classDiagram
  IMatrixFileProcessorFactory <.. MatrixOperations : list<AbstractMatrixFileProcessor> items

  class MatrixOperations {
    +create(operation) AbstractMatrixFileProcessor
    +operations() strings
  }

  MatrixOperations *-- TransposeMatrixFileProcessorFactory
  MatrixOperations *-- SumMatrixFileProcessorFactory
  MatrixOperations *-- DeterminantMatrixFileProcessorFactory

  class ArgumentParser {
    list<string> args
    +isArgsValid() bool
    +operation() string
    +sourceFilePath() string
    +targetFilePath() string
    +synopsis() string
  }
```
### Общая библиотека
```mermaid
  classDiagram

  class TextFile {
    string filepath
    string content

    +TextFile(filepath)
    +path() string
    +setContent(value)
    +getContent()
    +read() bool
    +write() bool
    +exists() bool
    +remove()
  }

  class Matrix3 {
    COLS static const int = 3
    ROWS static const int = 3

    +Matrix3()
    +fromVector(value) Matrix3 static
    +toVector() vector<double>
    +at(col, row) double
    +transposed() Matrix3
    +operator+(other) Matrix3
    +det() double
  }

  class Matrix3StringFormatter {
    +toString(matrix) string
    +fromString(value, out matrix) bool

    +toString(matrix1, matrix2) string
    +fromString(value, out matrix1, out matrix2) bool
  }
```

## Исходные файлы
### Функциональный тест
* func_tests/test_home_work.cpp

### Общая библиотека
#### Классы дла работы с матрицами и файлами
* source/common/Matrix3StringFormatter.h
* source/common/Matrix3StringFormatter.cpp
* source/common/TextFile.h
* source/common/TextFile.cpp
* source/common/Matrix3.h
* source/common/Matrix3.cpp
#### Тесты
* unit_tests/common/test_textfile.cpp
* unit_tests/common/test_matrix3.cpp
* unit_tests/common/test_matrix3stringformatter.cpp

### Приложение 1
#### Точка входа
* source/p1/main.cpp
* source/p1/main_app.h
* source/p1/main_app.cpp
#### Интерфейсы
* source/p1/IMatrixFileProcessorFactory.h
#### Абстрактные классы
* source/p1/AbstractMatrixFileProcessor.h
* source/p1/AbstractMatrixFileProcessor.cpp
#### Реализация операций над матрицами
* source/p1/TransposeMatrixFileProcessor.h
* source/p1/TransposeMatrixFileProcessor.cpp
* source/p1/SumMatrixFileProcessor.h
* source/p1/SumMatrixFileProcessor.cpp
* source/p1/DeterminantMatrixFileProcessor.h
* source/p1/DeterminantMatrixFileProcessor.cpp
#### Реализация фабрик операций над матрицами
* source/p1/TransposeMatrixFileProcessorFactory.h
* source/p1/TransposeMatrixFileProcessorFactory.cpp
* source/p1/SumMatrixFileProcessorFactory.h
* source/p1/SumMatrixFileProcessorFactory.cpp
* source/p1/DeterminantMatrixFileProcessorFactory.h
* source/p1/DeterminantMatrixFileProcessorFactory.cpp
#### Класс разбора аргументов командной строки
* source/p1/ArgumentParser.h
* source/p1/ArgumentParser.cpp
#### Класс агрегатор списка фабрик операций над матрицами
* source/p1/MatrixOperations.h
* source/p1/MatrixOperations.cpp
#### Тесты
* unit_tests/p1/test_argumentparser.cpp
* unit_tests/p1/test_determinantmatrixfileprocessorfactory.cpp
* unit_tests/p1/test_summatrixfileprocessorfactory.cpp
* unit_tests/p1/test_transposematrixfileprocessorfactory.cpp
* unit_tests/p1/test_matrixoperations.cpp
* unit_tests/p1/test_abstractmatrixfileprocessor.cpp
* unit_tests/p1/test_transposematrixfileprocessor.cpp
* unit_tests/p1/test_summatrixfileprocessor.cpp
* unit_tests/p1/test_determinantmatrixfileprocessor.cpp
* unit_tests/p1/test_main.cpp

### Приложение 2
#### Точка входа
* source/p2/main.cpp
* source/p2/main_app.h
* source/p2/main_app.cpp
#### Интерфейсы
* source/p2/ITargetOutput.h
#### Абстрактные классы
* source/p1/AbstractMatrixFileProcessor.h
* source/p1/AbstractMatrixFileProcessor.cpp
#### Реализация вывода в файл и адаптера запуска файла
* source/p2/FileTargetOutput.h
* source/p2/FileTargetOutput.cpp
* source/p2/RunableTargetOutput.h
* source/p2/RunableTargetOutput.cpp
#### Реализация фабричной функции
* source/p2/OutputFactory.h
* source/p2/OutputFactory.cpp
#### Класс разбора аргументов командной строки
* source/p2/ArgumentParser.h
* source/p2/ArgumentParser.cpp
#### Генератор матрицы 3х3 из случайных чисел
* source/p2/MatrixGenerator.h
* source/p2/MatrixGenerator.cpp
#### Тесты
* unit_tests/p2/test_argumentparser.cpp
* unit_tests/p2/test_matrixgenerator.cpp
* unit_tests/p2/test_filetargetoutput.cpp
* unit_tests/p2/test_runabletargetoutput.cpp
* unit_tests/p2/test_outputfactory.cpp
* unit_tests/p2/test_main.cpp
