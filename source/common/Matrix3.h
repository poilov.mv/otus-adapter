#pragma once

#include <vector>


class Matrix3 {
    std::vector<double> m;

public:
    static const int COLS = 3;
    static const int ROWS = 3;

    Matrix3();

    static Matrix3 fromVector(const std::vector<double> &value);
    std::vector<double> toVector() const;

    double &at(int col, int row);
    const double &at(int col, int row) const;

    Matrix3 transposed() const;
    Matrix3 operator+ (const Matrix3 &other) const;
    double det() const;
};
