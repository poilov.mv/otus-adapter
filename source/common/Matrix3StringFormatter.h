#pragma once

#include "Matrix3.h"
#include <string>


class Matrix3StringFormatter {
public:
    static std::string toString(const Matrix3 &matrix);
    static bool fromString(const std::string& value, Matrix3 *matrix);

    static std::string toString(const Matrix3 &matrix1, const Matrix3 &matrix2);
    static bool fromString(const std::string& value, Matrix3 *matrix1, Matrix3 *matrix2);
};
