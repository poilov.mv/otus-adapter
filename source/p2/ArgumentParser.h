#pragma once

#include <string>
#include <vector>

class ArgumentParser {
    std::vector<std::string> args;

public:
    ArgumentParser(int argc, char **argv);

    bool isArgsValid() const;

    bool hasRunable() const;
    std::string runablePath() const;
    std::string targetFilePath() const;

    std::string synopsis() const;
};
