#include "main_app.h"

#include "ArgumentParser.h"
#include "MatrixGenerator.h"
#include "OutputFactory.h"

#include <iostream>


int main_app(int argc, char **argv) {
    auto args = ArgumentParser(argc, argv);
    if (args.isArgsValid()) {
        auto outputTarget = createOutputTarget(args);

        try {
            auto matrix1 = generateMatrix3();
            auto matrix2 = generateMatrix3();

            outputTarget->write(matrix1, matrix2, args.targetFilePath());
        }
        catch (const TargetOutputExeption &ex) {
            std::cout << "Operation error: " << ex.what() << std::endl;
            return 2;
        }

        return 0;
    } else {
        std::cout << args.synopsis();
        return 1;
    }
}
