#include "ArgumentParser.h"

#include <sstream>


ArgumentParser::ArgumentParser(int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        args.push_back(argv[i]);
    }
}

bool ArgumentParser::isArgsValid() const {
    return 1 <= args.size() && args.size() <= 2;
}

std::string ArgumentParser::synopsis() const {
    std::stringstream ss;
    ss << "Usage:" << std::endl;
    ss << "p2.exe " << " <output_file> [path_to_runable]" << std::endl;

    return ss.str();
}

bool ArgumentParser::hasRunable() const {
    return args.size() > 1;
}

std::string ArgumentParser::runablePath() const {
    return args[1];
}

std::string ArgumentParser::targetFilePath() const {
    return args[0];
}
