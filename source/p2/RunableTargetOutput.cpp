#include "RunableTargetOutput.h"

#include "FileTargetOutput.h"
#include <cstdlib>


RunableTargetOutput::RunableTargetOutput(const std::string &runablePath)
    : runablePath(runablePath) {}

void RunableTargetOutput::write(
    const Matrix3 &matrix1,
    const Matrix3 &matrix2,
    const std::string &filePath) {
    FileTargetOutput fileOutput;
    fileOutput.write(matrix1, matrix2, filePath);

    auto quotedFilePath = "\"" + filePath + "\"";
#ifdef linux
    auto command = "sh " + runablePath + " " + quotedFilePath;
#else
    auto command = runablePath + " " + quotedFilePath;
#endif
    if (std::system(command.c_str()) != 0) {
        throw TargetOutputExeption("Error while running: " + command);
    }
}
