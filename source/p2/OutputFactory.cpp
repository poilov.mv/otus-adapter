#include "OutputFactory.h"

#include "RunableTargetOutput.h"
#include "FileTargetOutput.h"


std::unique_ptr<ITargetOutput> createOutputTarget(const ArgumentParser &args) {
    if (args.hasRunable()) {
        return std::make_unique<RunableTargetOutput>(args.runablePath());
    } else {
        return std::make_unique<FileTargetOutput>();
    }
}
