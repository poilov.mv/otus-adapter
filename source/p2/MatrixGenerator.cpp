#include "MatrixGenerator.h"

#include <algorithm>


struct DoubleGenerator {
    double operator()() {
        return double((std::rand() % 1'000) - 500) / 10.0;
    }
};

std::vector<double> generate(int count) { 
    std::vector<double> sourceNumbers;
    sourceNumbers.resize(count);
    std::generate_n(sourceNumbers.begin(), count, DoubleGenerator());
    return sourceNumbers; 
}

Matrix3 generateMatrix3() {
    return Matrix3::fromVector(generate(Matrix3::COLS * Matrix3::ROWS));
}
