#pragma once

#include "ITargetOutput.h"


class RunableTargetOutput
    : public ITargetOutput {
    std::string runablePath;

public:
    RunableTargetOutput(const std::string &runablePath);

    void write(
        const Matrix3 &matrix1, 
        const Matrix3 &matrix2, 
        const std::string &filePath) override;
};
