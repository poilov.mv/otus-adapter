#include "FileTargetOutput.h"

#include "TextFile.h"
#include "Matrix3StringFormatter.h"


void FileTargetOutput::write(
    const Matrix3 &matrix1,
    const Matrix3 &matrix2,
    const std::string &filePath) {
    auto content = Matrix3StringFormatter::toString(matrix1, matrix2);

    TextFile file(filePath);
    file.setContent(content);
    if (!file.write()) {
        throw TargetOutputExeption("Can not write file: " + filePath);
    }
}
