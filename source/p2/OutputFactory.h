#pragma once

#include "ArgumentParser.h"
#include "ITargetOutput.h"
#include <memory>


std::unique_ptr<ITargetOutput> createOutputTarget(const ArgumentParser &args);
