#pragma once

#include "Matrix3.h"
#include <string>
#include <stdexcept>


class TargetOutputExeption
    : public std::runtime_error {
public:
    explicit TargetOutputExeption(const std::string& _Message) 
        : std::runtime_error(_Message) {}
};


class ITargetOutput {
public:
    virtual ~ITargetOutput() = default;

    virtual void write(const Matrix3 &matrix1, const Matrix3 &matrix2, const std::string &filePath) = 0;
};
