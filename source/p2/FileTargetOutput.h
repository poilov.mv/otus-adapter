#pragma once

#include "ITargetOutput.h"


class FileTargetOutput
    : public ITargetOutput {
public:
    void write(
        const Matrix3 &matrix1, 
        const Matrix3 &matrix2, 
        const std::string &filePath) override;
};
