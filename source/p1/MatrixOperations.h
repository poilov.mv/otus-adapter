#pragma once

#include "IMatrixFileProcessorFactory.h"
#include <vector>


class MatrixOperations {
    typedef std::unique_ptr<IMatrixFileProcessorFactory> IMatrixFileProcessorFactoryUPtr;

    std::vector<IMatrixFileProcessorFactoryUPtr> items;

public:
    MatrixOperations();

    std::vector<std::string> operations() const;

    std::unique_ptr<AbstractMatrixFileProcessor> create(const std::string &operation);
};
