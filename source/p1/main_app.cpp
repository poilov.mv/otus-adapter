#include "main_app.h"

#include "ArgumentParser.h"
#include "MatrixOperations.h"

#include <iostream>


int main_app(int argc, char **argv) {
    MatrixOperations operationFabric;

    auto args = ArgumentParser(argc, argv);
    if (args.isArgsValid()) {
        auto matrixOperation = operationFabric.create(args.operation());

        try {
            matrixOperation->apply(args.sourceFilePath(), args.targetFilePath());
        }
        catch (const MatrixFileProcessorError &ex) {
            std::cout << "Operation error: " << ex.what() << std::endl;
            return 2;
        }

        return 0;
    } else {
        std::cout << args.synopsis(operationFabric.operations());
        return 1;
    }
}
