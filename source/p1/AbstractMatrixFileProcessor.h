#pragma once

#include <string>
#include <stdexcept>


class MatrixFileProcessorError
    : public std::runtime_error {
public:
    explicit MatrixFileProcessorError(const std::string& _Message);
};

class AbstractMatrixFileProcessor {
public:
    virtual ~AbstractMatrixFileProcessor() = default;

    void apply(
        const std::string &sourceFilePath,
        const std::string &targetFilePath);

protected:
    virtual std::string applyOp(const std::string &content) = 0;

private:
    std::string readFile(const std::string &sourceFilePath);
    void writeFile(const std::string &content, const std::string &targetFilePath);
};
