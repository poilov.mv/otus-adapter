#pragma once

#include "AbstractMatrixFileProcessor.h"


class SumMatrixFileProcessor
    : public AbstractMatrixFileProcessor {
protected:
    std::string applyOp(const std::string &content) override;
};
