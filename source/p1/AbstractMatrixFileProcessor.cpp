#include "AbstractMatrixFileProcessor.h"

#include "TextFile.h"


MatrixFileProcessorError::MatrixFileProcessorError(const std::string& _Message)
    : std::runtime_error(_Message) {}

void AbstractMatrixFileProcessor::apply(
    const std::string &sourceFilePath,
    const std::string &targetFilePath) {
    auto content = readFile(sourceFilePath);

    content = applyOp(content);

    writeFile(content, targetFilePath);
}

std::string AbstractMatrixFileProcessor::readFile(const std::string &sourceFilePath) {
    TextFile file(sourceFilePath);
    if (!file.read()) {
        throw MatrixFileProcessorError("Can not read file: " + sourceFilePath);
    }
    return file.getContent();
}

void AbstractMatrixFileProcessor::writeFile(const std::string &content, const std::string &targetFilePath) {
    TextFile file(targetFilePath);
    file.setContent(content);
    if (!file.write()) {
        throw MatrixFileProcessorError("Can not write file: " + targetFilePath);
    }
}
