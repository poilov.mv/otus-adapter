#pragma once

#include "AbstractMatrixFileProcessor.h"


class DeterminantMatrixFileProcessor
    : public AbstractMatrixFileProcessor {
protected:
    std::string applyOp(const std::string &content) override;
};
