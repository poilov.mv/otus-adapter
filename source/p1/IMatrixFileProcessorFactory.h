#pragma once

#include "AbstractMatrixFileProcessor.h"
#include <string>
#include <memory>

class IMatrixFileProcessorFactory {
public:
    virtual ~IMatrixFileProcessorFactory() = default;

    virtual std::unique_ptr<AbstractMatrixFileProcessor> create() = 0;

    virtual std::string operation() const = 0;
};
