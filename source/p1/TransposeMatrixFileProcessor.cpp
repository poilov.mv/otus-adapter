#include "TransposeMatrixFileProcessor.h"

#include "Matrix3StringFormatter.h"


std::string TransposeMatrixFileProcessor::applyOp(const std::string &content) {
    Matrix3 matrix;
    if (!Matrix3StringFormatter::fromString(content, &matrix)) {
        throw MatrixFileProcessorError("Wrong file format");
    }

    matrix = matrix.transposed();

    return Matrix3StringFormatter::toString(matrix);
}
