#include "DeterminantMatrixFileProcessorFactory.h"
#include "DeterminantMatrixFileProcessor.h"


std::unique_ptr<AbstractMatrixFileProcessor> DeterminantMatrixFileProcessorFactory::create() {
    return std::make_unique<DeterminantMatrixFileProcessor>();
}

std::string DeterminantMatrixFileProcessorFactory::operation() const {
    return "determinant";
}
