#include "TransposeMatrixFileProcessorFactory.h"
#include "TransposeMatrixFileProcessor.h"


std::unique_ptr<AbstractMatrixFileProcessor> TransposeMatrixFileProcessorFactory::create() {
    return std::make_unique<TransposeMatrixFileProcessor>();
}

std::string TransposeMatrixFileProcessorFactory::operation() const {
    return "transpose";
}
