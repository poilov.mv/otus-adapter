#include "gtest/gtest.h"

#include "TextFile.h"
#include "Matrix3StringFormatter.h"


class TestMain : public ::testing::Test {
protected:
    TextFile targetFile;
    TextFile runableFile;

    TestMain()
        : targetFile("file_with_sum.txt")
        , runableFile("some.cmd") {}

    void TearDown() override {
        targetFile.remove();
        runableFile.remove();
    }
};

std::string exe(const std::string &value) {
#ifdef linux
    return "./" + value;
#else
    return value;
#endif
}

std::string arg(const std::string &value) {
#ifdef linux
    return "$" + value;
#else
    return "%" + value;
#endif
}

TEST_F(TestMain, can_use_runable_for_transform) {
    auto content = exe("p1") + " sum " + arg("1") + " " + arg("1");
    runableFile.setContent(content);
    runableFile.write();

    auto command = exe("p2") + " " + targetFile.path() + " " + runableFile.path();
    ASSERT_EQ(0, std::system(command.c_str()));

    targetFile.read();
    auto resultContent = targetFile.getContent();
    Matrix3 matrix;
    ASSERT_TRUE(Matrix3StringFormatter::fromString(resultContent, &matrix));
}
